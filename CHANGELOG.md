## Changelog

### 0.7.0 (2020-06-14)
* Uploading supports selecting multiple files in one go.
* Failed uploads now have a "Retry" button.

### 0.6.0 (2020-05-09)
* We now have a [manual page](manual-page/qair.1.md).
* Closing `qair` (control+c) rolls up (hides) the QR code.
* `qair` is now also a library. This is used by
  [`gqair`](https://codeberg.org/willempx/gqair): `qair` with a GUI.
  `qair` as commandline utility will stay.
* Team up with the fork of the QR code terminal renderer.

### 0.5.0 (2020-04-04)
* Revised command line parameters (again...). Receiving files is not
  enabled by default anymore because software should only do what is asked.
  * To send: `qair FILE`
  * To receive: `qair -u`
  * Both: `qair -u FILE`
* Update crate dependencies (thanks to Laurent)
* Dropped support for running `qair` on Windows.
  Of course Windows clients can still connect to `qair`.

### 0.4.0 (2019-11-30)
* QR code by default points to HTML page with download button (instead of
  immediately to the file). Use `--deeplink` to disable.
* Simpler command line parameters: usually just `qair FILE` (send+receive)
  or `qair` (only receive).
  Old behavior of only sending available via `qair --no-receive FILE`.

### 0.3.0 (2019-08-31)
Features and improvements:
* Support resume on sending files
* Only show QR if starting webserver succeeded
* Choosing an unused filename for receiving files is now multi-process-safe
* Receiving files keeps filename (Windows not supported)

Under the hood:
* Update dependencies (thanks to Laurent)
* Switch to new HTTP server library

### 0.2.0 (2018-11-11)
* Fix IP address autodetection on OS X (thanks to anonymous contributor)
* Support for Windows
* Support for legacy Windows terminal (needed for older versions of Windows)
* Binaries for Linux (static linked) and Windows (both only i686 and amd64)


### 0.1.0 (2018-09-29)
First released version! This is mostly a Rust exercise. Rust-exercise-wise this is not
finished (many things are not yet done properly I think), but people are already using
it or want to use it, so let's release this!

New features:
* Sending files concurrently
* Simple progress bar in upload/receive HTML form
* Automatic IP address detection
* Support for terminals that display a gap between lines of █ blocks

