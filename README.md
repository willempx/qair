# `qair`

![Screenshot](https://www.willemp.be/qair/screenshots/screenshot-qair-0.4.0.png)

* To send files: `qair FILE`
* Or, to receive files: `qair -u`
* Scan the QR code with your smartphone
* Tap the download/upload button.

That's all.

No more hassle:
* **No cables**
* **No dedicated mobile app**: The QR barcode is standard, so you do not need to install a
  dedicated app on you smartphone.
  You can use the barcode scanner of your camera app or browser, or any QR code reading app.
* **No bluetooth pairing**
* **No signup, no account**
* **No internet server**. `qair` sends the file directly from your PC to your smartphone over
  HTTP over wifi. Your files do not go through a server/cloud. Your smartphone usually needs to be on the
  same wifi as your PC.


## Install
* Linux: [Download binaries](https://codeberg.org/willempx/qair/releases)
* MacOS X: [Install rust](https://www.rust-lang.org/) and run `cargo install qair`
* Windows: Not supported anymore. Windows clients can still connect of course.

