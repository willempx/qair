#!/bin/bash
#
# If you just want to build qair from source, you don't need this script and you can just use
#    cargo build
# for a debug build, or
#    cargo build --release
# for a release build.
#
# Still here? okay then... This script is the ducktape to create packages.
# This supports reproducible builds, i.e. making a package twice yields the exact same file,
# as long as the same compiler version is used.
#
# This script assumes to run on Linux x86_64.
# use 'SKIP_LESS_CACHABLE_CHECKS=true' to skip checks that are slow and hard to cache.
#


appname() {
    # Search for 'name = "name_of_the_application"' in Cargo.toml
    grep -Po 'name\s*=\s*"\K[a-zA-Z_\-]+(?=")' Cargo.toml
}

# Prints the version, e.g. "0.1" or "0.2.0-2-g3d0c729-dirty".
version() {
    git describe --tags --dirty --always
}

time_of_commit() {
    git log -1 --pretty=%cI
}

# Show the first argument in a fancy color followed by the rest uncolored
# without newline inbetween, but with newline at the end.
show_colored() {
    echo -en "\033[1;36;40m$1\033[0m" > /dev/stderr
    shift
    echo $@ >/dev/stderr
}

show_and_run() {
    show_colored '$ ' $@
    "$@"
}

format_manual_page(){
    show_and_run pandoc --standalone --to man manual-page/qair.1.md -o manual-page/qair.1
}

# Creates .tar.gz based one existing build output and manual page.
package_tgz() {
    target_triple="$1"
    pretty_target_name="$2"
    
    show_and_run tar \
        --mtime="$(time_of_commit)" \
        --transform="s,target/$target_triple/release/,$(appname)-$(version)/," \
        --transform="s,manual-page/,$(appname)-$(version)/manual-page/," \
        -c \
        "target/$target_triple/release/$(appname)" \
        "manual-page/qair.1" \
        | gzip -n > "$(appname)-$(version)-$pretty_target_name".tar.gz
}

install_rust() {
    if [ "$SKIP_LESS_CACHABLE_CHECKS" != "true" ]; then
        show_colored '--> ' "Installing rust"
        curl https://sh.rustup.rs -sSf | sh -s -- -y
        show_and_run source $HOME/.cargo/env
    fi
}

check_can_publish() {
    if [ "$SKIP_LESS_CACHABLE_CHECKS" != "true" ]; then
        show_and_run cargo publish --dry-run --allow-dirty
    fi
}

package() {
    target=$1      # target triple, e.g. x86_64-unknown-linux-musl
    runtest=$2     # "true" or "false"
    packagename=$3 # basically the friendly name of the target triple.
    
    show_colored '--> ' "Creating package for $target ($packagename)"
    
    show_and_run rustup target add $target
    if [ "$runtest" != "false" ]; then
        show_and_run cargo test --release --target=$target
    fi
    show_and_run cargo build --release --target=$target
    package_tgz $target $packagename
}

check_correct_dir() {
    if [ ! -f Cargo.toml ] ; then
        echo "Error: script must be run from the root directory of the program." 2>/dev/stderr
        exit 1
    fi
}

main() {
    # exit immediately when a command fails.
    set -e
    
    check_correct_dir
    
    install_rust
    
    # done first to avoid 2x 'updating crates.io index'
    check_can_publish
    
    format_manual_page

    package x86_64-unknown-linux-musl true linux-x86_64
    package i686-unknown-linux-musl false linux-i686
}

main
