/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

//! HTTP handler to send a HTTP 403 with a non-empty message because empty errors are confusing.
//!
//! Can be both used as a handler on its own (i.e. route a path to this handler),
//! or to call inside another handler.

extern crate hyper;
extern crate iron;
extern crate mount;
extern crate percent_encoding;
extern crate router;

use iron::request::Request;
use iron::response::Response;
use iron::{status, IronResult};

/// Message sent to HTTP client when it requests a file that has not been shared
/// and gets a HTTP 403.
static NOT_SHARED_MESSAGE: &str =
    "403 Forbidden. The file is not shared with you, or does not exist, or cannot be opened.";

/// Handler for HTTP requests of files that are not shared/served.
///
/// By default Iron sends a white page on errors, which is confusing to users.
/// This function replaces that white page with an error message.
///
/// For files that are not provided we do not differentiate between whether it does not exists
/// (HTTP 403) and whether it has no permission (HTTP 401) for security reasons:
/// we do not want to leak the information which files exists and which ones do not.
pub fn not_shared_handler(_request: &mut Request) -> IronResult<Response> {
    Ok(Response::with((status::Forbidden, NOT_SHARED_MESSAGE)))
}
