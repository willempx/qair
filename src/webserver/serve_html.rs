/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

//! HTTP handler to send HTML stored in memory, containing the upload form
//! and/or download button.
//!
//! The purpose of the upload form is that the user can upload files through
//! the webbrowser.
//!
//! The purpose of having a HTML page with a download button instead of
//! deeplinking avoids issues on some Android devices.

extern crate bart;
extern crate bart_derive;
extern crate hyper;
extern crate iron;
extern crate mount;
extern crate percent_encoding;
extern crate router;

use self::hyper::mime::{Mime, SubLevel, TopLevel};
use iron::middleware::Handler;
use iron::request::Request;
use iron::response::Response;
use iron::{headers, status, IronResult};
use std::path::{Path, PathBuf};

/// Filename to be used in case the actual filename is not a valid UTF-8.
static FALLBACK_FILENAME: &'static str = "file_with_unformattable_name.dat";

/// Fields to fill in in the HTML template.
#[derive(BartDisplay)]
#[template = "src/template.html"]
struct Template {
    download_filename: Option<String>,
    upload: bool,
}

/// HTTP handler to send HTML stored in memory, containing the upload form
/// and download button.
pub struct ServeHtml {
    html: String,
}

/// Turns "/home/john/projects/project1/file.txt" into "file.txt".
/// If not possible, returns a fallback filename.
fn basename(path: &PathBuf) -> String {
    if let Some(os_str) = Path::new(&path).file_name() {
        os_str.to_str().unwrap_or(FALLBACK_FILENAME).to_string()
    } else {
        FALLBACK_FILENAME.to_string()
    }
}

impl ServeHtml {
    /// Creates a new HTTP handler for the HTML containing the upload form and/or download button.
    ///
    /// If `path` is `None`, no download button is in the HTML.
    /// If `receive` is true, an upload form is in the HTML.
    pub fn new(path: &Option<PathBuf>, receive: bool) -> ServeHtml {
        let template = &Template {
            download_filename: path.as_ref().map(basename),
            upload: receive,
        };
        let html = format!("{}", &template);
        ServeHtml { html: html }
    }
}

impl Handler for ServeHtml {
    /// Handler for HTTP requests to serve the HTML containing upload form and/or download button.
    fn handle(&self, _req: &mut Request) -> IronResult<Response> {
        let mut response = Response::with((status::Ok, self.html.as_str()));
        let header = headers::ContentType(Mime(TopLevel::Text, SubLevel::Html, vec![]));
        response.headers.set(header);
        Ok(response)
    }
}
