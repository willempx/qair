/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

//! Console/terminal QR code rendering is now here: https://gitlab.com/timvisee/qr2term-rs
//!
//! What is left in this file is what's specific to `qair`, the rest has moved to `qr2term` crate.

use qr2term::matrix::Matrix;
use qr2term::render;

/// A QR code that can be printed.
pub struct Qr {
    matrix: Matrix<render::Color>,
}

/// How much white around the QR code.
///
/// Should be 4, but 1 works fine and we don't have much space.
/// (see https://qrworld.wordpress.com/2011/08/09/the-quiet-zone/)
const QUIET_ZONE_WIDTH: usize = 1;

impl Qr {
    /// Prepares printing QR code.
    ///
    /// After this, you probably want to call `print_qr`.
    /// These are separate functions to facilitate more complex error handling.
    pub fn new<D: AsRef<[u8]>>(data: D) -> Result<Self, String> {
        let raw = qr2term::qr::Qr::from(&data)
            .map_err(|err| format!("Error: Cannot print QR code: {}", err))?;
        let mut matrix = raw.to_matrix();
        matrix.surround(QUIET_ZONE_WIDTH, render::QrLight);
        Ok(Qr { matrix })
    }

    /// Prints the QR code`.
    ///
    /// # Returns
    /// the number of (columns, rows) it took on the terminal.
    pub fn print(&self) -> (usize, usize) {
        let renderer = render::Renderer::default();
        renderer.print_stdout(&self.matrix);
        (renderer.width(&self.matrix), renderer.height(&self.matrix))
    }
}
