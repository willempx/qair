/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

//! # `qair`
//!
//! ![Screenshot](https://www.willemp.be/qair/screenshots/screenshot-qair-0.4.0.png)
//!
//! Send/receive files with builtin HTTP server and terminal QR code.

extern crate structopt;
#[macro_use]
extern crate bart_derive;
extern crate iron;
extern crate qr2term;
extern crate router;

mod cli;
mod cli_arguments;
mod console_barcode_renderer;
mod myip;
mod noclobber;
mod qair;
mod util;
mod webserver;

fn main() {
    cli::run()
}
