/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

//! Interact with user through command line and terminal and launches webserver.

use crate::cli_arguments;
use crate::console_barcode_renderer::Qr;
use crate::qair;
use crate::util;
use crossterm::{cursor, terminal, QueueableCommand};
use std::convert::TryInto;
use std::io::{stdout, Write};
use structopt::StructOpt;

/// Parse command line arguments and do what is asked in these arguments
///
/// Basically `main`.
pub fn run() {
    // Parse command line arguments.
    let arguments = cli_arguments::Arguments::from_args();
    match run_with_args(&arguments) {
        Ok(()) => (),
        Err(msg) => {
            eprintln!("{}", msg);
            std::process::exit(1);
        }
    }
}

/// Like the `main` function but gets parsed command line arguments as argument.
pub fn run_with_args(args: &cli_arguments::Arguments) -> Result<(), String> {
    args.check_constraints()?;

    if let Some(text) = &args.show {
        run_for_qr_rendering(&text)
    } else {
        run_for_file_transfer(args)
    }
}

/// Run the program for the QR-render-only mode (user does not want file transfer).
///
/// # Panics
/// Given commandline arguments are not for QR-render-only mode (`args.show` must not be `None`).
fn run_for_qr_rendering(text: &str) -> Result<(), String> {
    let qr = Qr::new(&text)?;
    qr.print();
    Ok(())
}

/// Run the program for the file transfer mode. Blocks.
///
/// There are multiple modes: to transfer files, and to only show a QR code without filetrasfer.
/// This function is for the file transfer mode.
///
/// # Panics
/// Given commandline arguments are not for file transfer mode (`args.show` must be `None`).
fn run_for_file_transfer(args: &cli_arguments::Arguments) -> Result<(), String> {
    args.check_constraints()?;
    assert!(args.show == None);

    // Try to get early errors (permissions, etc).
    // The file will be re-opened when sending it to prevent sending replaced file in case
    //     of e.g. "mv newfile filetosend".
    if let Some(file) = &args.file {
        util::verify_file_openable(&file)?;
    }

    let qair = cli_args_to_qair(args);
    let url = qair.url().map_err(|_| format!("Cannot format URL"))?;
    let qr = Qr::new(&url).map_err(|err| format!("Error: {}", err))?;
    let webserver = qair
        .start()
        .map_err(|err| format!("Error: cannot start webserver: {}", err))?;
    listening(&qr, url);
    let _ = webserver; // dropping is blocking ; do not drop before the end.
    Ok(())
}

/// Converts commandline arguments into qair setup/config.
fn cli_args_to_qair(args: &cli_arguments::Arguments) -> qair::Qair {
    let mut qair = qair::Qair::new();
    qair.set_file_to_send(&args.file);
    qair.set_is_receiving_enabled(args.receive);
    qair.set_always_index(args.index);
    let ip_autodetection_config = match &args.host {
        None => qair::IpAutodetectionConfig::OnlyAutodetect,
        Some(host) => qair::IpAutodetectionConfig::AlwaysCustom(host.to_string()),
    };
    qair.set_ip_autodetection_config(ip_autodetection_config);
    qair.set_port(args.port);
    qair
}

/// What to do while the webserver is running.
///
/// - show QR code
/// - show URL
/// - hide those again when ctrl+c is pressed.
fn listening(qr: &Qr, url: String) {
    let (qr_width, qr_height) = qr.print();
    println!("{}", &url);
    let _ = ctrlc::set_handler(move || ctrl_c_handler(qr_width, qr_height, &url));
}

/// What to do when ctrl+c is pressed: wipe QR code etc and exit.
fn ctrl_c_handler(qr_width: usize, qr_height: usize, url: &str) {
    let clear_lines = lines_to_roll_up(qr_width, qr_height, &url);
    let _ = roll_up(clear_lines);
    std::process::exit(0);
}

/// Wipes the last `clear_lines` lines from the terminal (and move cursor up).
///
/// Does nothing if `clear_lines` is zero (no I/O).
fn roll_up(clear_lines: u16) -> crossterm::Result<()> {
    if clear_lines == 0 {
        return Ok(());
    }
    let mut stdout = stdout();
    stdout.queue(cursor::MoveToColumn(0))?;
    stdout.queue(cursor::MoveUp(clear_lines))?;
    stdout.queue(terminal::Clear(terminal::ClearType::FromCursorDown))?;
    stdout.flush()?;
    Ok(())
}

/// How many lines do we have to roll up to clear out output
///
/// This is imperfect because terminals are imperfect:
/// - We cannot know whether the terminal has word-wrap (we assume it does),
/// - the terminal can resize while we do our calculation (basically a race condition),
/// - we cannot clear part of the scrollback buffer.
///
/// Returns 0 if you'd beter not roll up.
fn lines_to_roll_up(qr_width: usize, qr_height: usize, url: &str) -> u16 {
    let term_width = match terminal::size() {
        Ok((width, _)) => usize::from(width),
        Err(_) => return 0,
    };
    let current_row = match cursor::position() {
        Ok((_, row)) => usize::from(row),
        Err(_) => return 0,
    };
    let used_height_url = ceil_div(url.len(), term_width);
    let used_height_qr = ceil_div(qr_width, term_width) * qr_height;
    let used_height = used_height_url + used_height_qr;
    if used_height > current_row {
        // Terminal do not support clearing part of the scrollback buffer.
        // Rather than doing a bad job clearing, do not clear.
        return 0;
    }
    return used_height.try_into().unwrap_or(0);
}

/// Implements `/^`. `x /^ y` is like `x / y` but "rounded" up instead of down.
///
/// For example, `1 /^ 2 == 1`, `10 /^ 3 == 4`, `10 /^ 5 == 2`.
///
/// Not generic until `std::num::Zero` has landed.
fn ceil_div(x: usize, y: usize) -> usize {
    x / y + if x % y == 0 { 0 } else { 1 }
}
