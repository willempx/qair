% QAIR(1) | qair, a file transfer tool

<!--
To convert this to a real unix-style manpage, run
   pandoc --standalone --to man qair.1.md -o qair.1
To view that manpage:
   man ./qair.1
Based on template from https://eddieantonio.ca/blog/2015/12/18/authoring-manpages-in-markdown-with-pandoc/
-->

NAME
====

**qair** -- file transfer tool

SYNOPSIS
========

| **qair** \[_FLAG_\]... \[_OPTION_\]... \[_FILE_\]

DESCRIPTION
===========

**qair** is a tool to send and/or receives files between two computers.
Only one of them needs to run **qair**. It needs neither internet-server
nor cloud.

Only your computer needs to run **qair**. The computer/phone not running
**qair** can simply use a webbrower, or any HTTP client, including
**wget(1)** and **curl(1)**.

The other computer/phone connects directly to your computer: there is no
server inbetween. There is no cloud service used. You are independent.

For sending a file, **qair** by default displays the URL (for example
http://192.168.1.60:1234/wildlife.mp4) both as human-readable text and as QR code. A QR code is a
two dimensional barcode that can be scanned with a camera, such as the camera of a smartphone.
This QR code is displayed on the terminal and works with most UTF-8 capable terminals.

Receiving files is similar, but **qair** sends an upload-form to the other
computer/phone which can be used to upload files to the computer running
**qair**. For details regarding receiving files, see the `-u` option below.

To close/quit/exit **qair**, press Control+c.

The files sent/received are not encrypted, so an advanced enough attacker can see
what files you send/receive and modify them while they are in transit. So be cautious when
sending/receiving files containing secrets or containing executable code.

Examples
--------

- `qair wildlife.mp4` : Send the file `wildlife.mp4`.
- `qair -u` : Receive files.


Options and flags
-----------------

`-h`, `--help`

:   Print an overview of the options (such as `-h`) with information which option does what.

`--host` _host_

:   Use given hostname/IP in URL instead of autodetecting

    Example: `qair --host 123.124.125.126 song.ogg`
    
    Example: `qair --host example.org -u`
    
    Without `--host` **qair** autodetects the IP address of your computer for printing
    the URL and QR code. With `--host`, **qair** instead uses the given IP address
    or hostname.

`-i`, `--index`

:   Do not deeplink in URL in QR code, link to an index.html page.

    When using `-i` or `--index`, a URL is printed that does not contain the filename. An example
    of such a URL is http://192.168.1.60:1234. When accessing the URL, the file
    is not sent, but a page (in HTML format) which contains a URL with
    the filename. An example of such a URL is http://192.168.1.60:1234/wildlife.mp4.

    When not using `-i` or `--index`, a URL is printed that contains the filename.

`-p` _port_, `--port` _port_

:   Listen on given port.

    Example: `qair -p 8080 song.ogg`

    If this option is not used, **qair** listens on port 1234.
    
    It is recommended to use port numbers below 32767. Higher port numbers are typically used
    by the operating system when setting up outgoing connections, so when using a higher port number
    it can happen that the port cannot be opened because the port is already in use, even
    though the user never requested to use the port before.
    
    When the port cannot be opened, **qair** quits immediately at startup with an error,
    without trying another port and without sending/receiving files. This implies that when
    using two instances of **qair** on the same computer, you'll have to use the `-p` option.
    
`--show` _text_

:   Show QR of given _text_ (do not send/receive files)

    Example: `qair --show https://example.org/distro.iso`
    
    Example: `qair --show "Hello, world!"`

    This allows **qair** to be used as a QR code printer.
   
`-u`

:   Allow receiving files (clients can upload). Implies `-i`.

    Example: To allow others to upload files to you: `qair -u`
    
    Example: As an alternative to a webbrowser, you can upload a file to **qair**
      like this: `curl --upload-file file.txt http://192.168.1.7:1234/`

    With `-u`, **qair** sends an upload form which can be used to upload
    files from the computer/phone not running **qair** to the computer
    running **qair**.
    
    Files are stored in the current working directory.
    
    Files received are guaranteed to have a different name of each other, and of existing
    files, also when running multiple instances of **qair**. The first consequence is that
    files will not get corrupted when uploading two files with the same name at the same time.
    The second consequence is that existing files are never overwritten.

    Files keep their filename, except:
    
    - To avoid overwriting files (see above), an incrementing number can be added. So `flower.png`
      can become `flower.png.1`, `flower.png.2`, etc.
    - When the file starts with a dot. This is for security reasons: people could take over
      your computer if you would allow uploading `.bash_profile` if your home directory is
      your current working directory, so we do not allow that. The file will get an underscore
      in the beginning (`_.bash_profile`).
    - When the file cannot be encoded in the encoding of the filesystem or in UTF-8.
    
    If receiving file is aborted (for example, the computer/phone
    connecting to **qair** crashes), then what is received so far is just
    kept (the file does not get deleted).
 
`-V`, `--version`

:   Print which version of **qair** you have.

BUGS
====

See Codeberg Issues: <https://codeberg.org/willempx/qair/issues>

AUTHOR
======

Willem Penninckx (willem(at)willemp(dot)be) and contributors.

